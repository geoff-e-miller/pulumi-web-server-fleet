package main

import (
	"pulumi-web-server-fleet/fleet"
	"pulumi-web-server-fleet/network"
	"pulumi-web-server-fleet/tagging"

	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		// awsConfig := config.New(ctx, "")
		// accessKey := os.Getenv("AWS_ACCESS_KEY_ID")
		// secretKey := os.Getenv("AWS_SECRET_ACCESS_KEY")
		// region := awsConfig.Require("aws:region")

		// awsProvider, err := aws.NewProvider(ctx, "awsprovider", &aws.ProviderArgs{
		// 	// AccessKey: pulumi.String(accessKey),
		// 	// SecretKey: pulumi.String(secretKey),
		// 	// Region:    pulumi.String(region),
		// })
		// if err != nil {
		// 	return err
		// }

		// Automatically inject tags.
		tagging.RegisterAutoTags(ctx, map[string]string{
			"iac.pulumi.organization":          ctx.Organization(),
			"iac.pulumi.project":               ctx.Project(),
			"iac.pulumi.stack":                 ctx.Stack(),
			"iac.pulumi.src.author":            "Geoff Miller",
			"PulumiCodingExerciseOptionNumber": "3",
			"PulumiCodingExerciseOptionName":   "Web Server Fleet Abstraction",
		})

		networkConfig := &network.NetworkConfig{}
		conf := config.New(ctx, "")
		conf.RequireObject("network", networkConfig)

		azs := make([]network.AZArgs, len(networkConfig.AZs))
		for i, az := range networkConfig.AZs {
			azs[i] = network.AZArgs{
				ZoneID:            az.ZoneID,
				SubnetCidrPublic:  az.SubnetCidrPublic,
				SubnetCidrPrivate: az.SubnetCidrPrivate,
			}
		}

		vpc, err := network.NewVPC(ctx, networkConfig.Name, &network.VPCArgs{
			Name:    networkConfig.Name,
			VpcCidr: networkConfig.VpcCidr,
			AZs:     azs,
		})
		if err != nil {
			return err
		}

		fleetConfig := &fleet.FleetConfig{}
		conf.RequireObject("fleet", fleetConfig)

		_, err = fleet.NewWebServerFleet(ctx, fleetConfig.Label, &fleet.WebServerFleetArgs{
			Label:           fleetConfig.Label,
			VpcId:           vpc.Vpc.ID(),
			SubnetIdsPublic: vpc.PublicSubnetIDs,
			SubnetIdsFleet:  vpc.PrivateSubnetIDs,
			Machines:        fleetConfig.Machines,
		})
		if err != nil {
			return err
		}

		return nil
	})
}
