#!/bin/bash

# Configuration variables
CONTAINER_NAME="my-hello-world"

# Check if Docker is installed
if ! command -v docker &> /dev/null
then
    echo "Docker is not installed. Installing now..."
    sudo yum update -y
    sudo amazon-linux-extras install docker -y
    sudo service docker start
    sudo usermod -a -G docker ec2-user
else
    echo "Docker is already installed."
fi

# Pull the Docker Hello World image
sudo docker pull httpd:2.4

# Run a Docker container using the Hello World image
sudo docker run -d --name $CONTAINER_NAME -p 80:80 httpd:2.4

# Create a simple index.html
echo '<html><body><h1>Hello, World!</h1></body></html>' | sudo tee /var/www/html/index.html

# Copy the index.html file to the Docker container
sudo docker cp /var/www/html/index.html $CONTAINER_NAME:/usr/local/apache2/htdocs/
