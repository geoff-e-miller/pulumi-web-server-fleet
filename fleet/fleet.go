package fleet

import (
	"encoding/base64"
	"errors"
	"fmt"
	"os"

	"github.com/pulumi/pulumi-aws/sdk/v5/go/aws/autoscaling"
	"github.com/pulumi/pulumi-aws/sdk/v5/go/aws/ec2"
	"github.com/pulumi/pulumi-aws/sdk/v5/go/aws/kms"
	"github.com/pulumi/pulumi-aws/sdk/v5/go/aws/lb"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// InstanceSizeMap defines the mapping between human readable sizes and AWS specific instance sizes
var InstanceSizeMap = map[string]string{
	"small":  "t2.medium",
	"medium": "t2.large",
	"large":  "t2.xlarge",
}

// AmiIdMap defines the mapping between OS and the corresponding AMI ID
var AmiIdMap = map[string]string{
	"windows": "ami-12345",
	"linux":   "ami-54321",
}

// UserDataByOsMap is map that holds the base64-encoded user data for each OS.
var UserDataByOsMap = map[string]pulumi.String{
	"windows": filebase64OrPanic("userdata-windows.ps1"),
	"linux":   filebase64OrPanic("userdata-linux.sh"),
}

// filebase64OrPanic reads the contents of a file at the specified path and returns the base64-encoded string representation.
// If the file cannot be read or an error occurs, it panics with the corresponding error message.
func filebase64OrPanic(path string) pulumi.String {
	if fileData, err := os.ReadFile(path); err == nil {
		return pulumi.String(base64.StdEncoding.EncodeToString(fileData[:]))
	} else {
		panic(err.Error())
	}
}

// WebServerFleetArgs holds the argument structure for NewWebServerFleet
type WebServerFleetArgs struct {
	Label           string
	VpcId           pulumi.StringInput
	SubnetIdsPublic pulumi.StringArrayInput
	SubnetIdsFleet  pulumi.StringArrayInput
	Machines        []MachineConfig
}

// WebServerFleet is the component that holds the state of the created resources for the fleet
type WebServerFleet struct {
	pulumi.ResourceState
	KmsKey                  *kms.Key
	LoadBalancer            *lb.LoadBalancer
	LoadBalancerTargetGroup *lb.TargetGroup
	LaunchTemplateMap       map[string]*ec2.LaunchTemplate
	AsgMap                  map[string]*autoscaling.Group
	SecurityGroupForFleet   *ec2.SecurityGroup
	SecurityGroupForLb      *ec2.SecurityGroup
}

// FleetConfig is a configuration for a fleet of machines.
// It includes a label for identification, the layer of subnets where the machines should be located,
// like public or private.
// and a slice of MachineConfig which describes each machine in the fleet.
type FleetConfig struct {
	Label       string          `json:"label"`        // The label for identification of the fleet.
	SubnetLayer string          `json:"subnet_layer"` // The layer of subnets where the machines should be located.
	Machines    []MachineConfig `json:"machines"`     // The configurations for each machine in the fleet.
}

// MachineConfig describes the configuration of a single machine in a fleet.
// It includes the OS the machine should use, the size of the machine, and the number of instances of this configuration.
type MachineConfig struct {
	OS    string `json:"os"`    // The OS the machine should use.
	Size  string `json:"size"`  // The size of the machine.
	Count int    `json:"count"` // The number of instances of this configuration.
}

// NewWebServerFleet creates a new WebServerFleet resource with the given name and arguments.
// It returns an instance of WebServerFleet and any error encountered.
func NewWebServerFleet(ctx *pulumi.Context, name string, args *WebServerFleetArgs, opts ...pulumi.ResourceOption) (*WebServerFleet, error) {
	// fleet represents a new WebServerFleet.
	fleet := &WebServerFleet{}
	// Register the component resource with Pulumi runtime.
	err := ctx.RegisterComponentResource("fleet:index:WebServerFleet", name, fleet, opts...)
	if err != nil {
		return nil, err
	}

	// Initialize
	fleet.LaunchTemplateMap = make(map[string]*ec2.LaunchTemplate)
	fleet.AsgMap = make(map[string]*autoscaling.Group)

	// Validation: check that each machine in the fleet has a valid OS and size.
	for _, machine := range args.Machines {
		_, ok := AmiIdMap[machine.OS]
		if !ok {
			return nil, errors.New("invalid OS")
		}

		_, ok = InstanceSizeMap[machine.Size]
		if !ok {
			return nil, errors.New("invalid size")
		}
	}

	// Create a new KMS key for encrypting volumes.
	fleet.KmsKey, err = kms.NewKey(ctx, args.Label, &kms.KeyArgs{
		DeletionWindowInDays: pulumi.Int(10),
		Description:          pulumi.String("KMS key 1"),
		Tags: pulumi.StringMap{
			"Name":  pulumi.String(args.Label),
			"Fleet": pulumi.String(args.Label),
		},
	})
	if err != nil {
		return nil, err
	}

	// Create the Security Groups
	fleet.SecurityGroupForLb, err = ec2.NewSecurityGroup(ctx, fmt.Sprintf("lb-%s", args.Label), &ec2.SecurityGroupArgs{
		Name: pulumi.Sprintf("lb.%s", args.Label),
		Ingress: ec2.SecurityGroupIngressArray{
			&ec2.SecurityGroupIngressArgs{
				FromPort:    pulumi.Int(443),
				ToPort:      pulumi.Int(443),
				CidrBlocks:  pulumi.StringArray{pulumi.String("0.0.0.0/0")},
				Protocol:    pulumi.String("tcp"),
				Description: pulumi.String(""),
			},
		},
		Egress: ec2.SecurityGroupEgressArray{
			&ec2.SecurityGroupEgressArgs{
				FromPort:    pulumi.Int(0),
				ToPort:      pulumi.Int(0),
				CidrBlocks:  pulumi.StringArray{pulumi.String("0.0.0.0/0")},
				Protocol:    pulumi.String("-1"),
				Description: pulumi.String(""),
			},
		},
		VpcId: args.VpcId,
		Tags: pulumi.StringMap{
			"Name":  pulumi.Sprintf("lb.%s", args.Label),
			"Fleet": pulumi.String(args.Label),
		},
	})
	if err != nil {
		return nil, err
	}

	fleet.SecurityGroupForFleet, err = ec2.NewSecurityGroup(ctx, fmt.Sprintf("fleet-%s", args.Label), &ec2.SecurityGroupArgs{
		Name: pulumi.Sprintf("fleet.%s", args.Label),
		Ingress: ec2.SecurityGroupIngressArray{
			&ec2.SecurityGroupIngressArgs{
				FromPort:       pulumi.Int(80),
				ToPort:         pulumi.Int(80),
				SecurityGroups: pulumi.StringArray{fleet.SecurityGroupForLb.ID()},
				Protocol:       pulumi.String("tcp"),
				Description:    pulumi.String(""),
			},
		},
		Egress: ec2.SecurityGroupEgressArray{
			&ec2.SecurityGroupEgressArgs{
				FromPort:    pulumi.Int(0),
				ToPort:      pulumi.Int(0),
				CidrBlocks:  pulumi.StringArray{pulumi.String("0.0.0.0/0")},
				Protocol:    pulumi.String("-1"),
				Description: pulumi.String(""),
			},
		},
		VpcId: args.VpcId,
		Tags: pulumi.StringMap{
			"Name":  pulumi.Sprintf("fleet.%s", args.Label),
			"Fleet": pulumi.String(args.Label),
		},
	})
	if err != nil {
		return nil, err
	}

	// Create the ELB
	fleet.LoadBalancer, err = lb.NewLoadBalancer(ctx, fmt.Sprintf("public-%s", args.Label), &lb.LoadBalancerArgs{
		Name:             pulumi.Sprintf("public-%s", args.Label),
		LoadBalancerType: pulumi.String("application"),
		Subnets:          args.SubnetIdsPublic,
		SecurityGroups: pulumi.StringArray{
			fleet.SecurityGroupForLb.ID(),
		},
		Internal:    pulumi.Bool(false),
		EnableHttp2: pulumi.Bool(true),
		Tags: pulumi.StringMap{
			"Name":  pulumi.Sprintf("public.%s", args.Label),
			"Fleet": pulumi.String(args.Label),
		},
	})
	if err != nil {
		return nil, err
	}

	fleet.LoadBalancerTargetGroup, err = lb.NewTargetGroup(ctx, fmt.Sprintf("public-%s", args.Label), &lb.TargetGroupArgs{
		Name:       pulumi.Sprintf("public-%s", args.Label),
		TargetType: pulumi.String("instance"),
		Port:       pulumi.Int(80),
		Protocol:   pulumi.String("HTTP"),
		VpcId:      args.VpcId,
		Tags: pulumi.StringMap{
			"Name":  pulumi.Sprintf("public.%s", args.Label),
			"Fleet": pulumi.String(args.Label),
		},
	}, pulumi.Parent(fleet.LoadBalancer))
	if err != nil {
		return nil, err
	}

	_, err = lb.NewListener(ctx, fmt.Sprintf("http-forward-https-public-%s", args.Label), &lb.ListenerArgs{
		LoadBalancerArn: fleet.LoadBalancer.Arn,
		Port:            pulumi.Int(80),
		Protocol:        pulumi.String("HTTP"),
		DefaultActions: lb.ListenerDefaultActionArray{
			lb.ListenerDefaultActionArgs{
				Order: pulumi.Int(1),
				Type:  pulumi.String("redirect"),
				Redirect: lb.ListenerDefaultActionRedirectArgs{
					Host:       pulumi.String("/#{host}"),
					Path:       pulumi.String("/#{path}"),
					Port:       pulumi.String("443"),
					Protocol:   pulumi.String("HTTPS"),
					Query:      pulumi.String("#{query}"),
					StatusCode: pulumi.String("HTTP_301"),
				},
			},
		},
		Tags: pulumi.StringMap{
			"Name":  pulumi.Sprintf("http_forward_https.public.%s", args.Label),
			"Fleet": pulumi.String(args.Label),
		},
	}, pulumi.Parent(fleet.LoadBalancer))
	if err != nil {
		return nil, err
	}

	_, err = lb.NewListener(ctx, fmt.Sprintf("https-public-%s", args.Label), &lb.ListenerArgs{
		LoadBalancerArn: fleet.LoadBalancer.Arn,
		Port:            pulumi.Int(443),
		Protocol:        pulumi.String("HTTPS"),
		DefaultActions: lb.ListenerDefaultActionArray{
			lb.ListenerDefaultActionArgs{
				Order:          pulumi.Int(1),
				Type:           pulumi.String("forward"),
				TargetGroupArn: fleet.LoadBalancerTargetGroup.Arn,
			},
		},
		Tags: pulumi.StringMap{
			"Name":  pulumi.Sprintf("https.public.%s", args.Label),
			"Fleet": pulumi.String(args.Label),
		},
	}, pulumi.Parent(fleet.LoadBalancer))
	if err != nil {
		return nil, err
	}

	// Create Launch Templates
	for os, ami := range AmiIdMap {
		for size, instanceType := range InstanceSizeMap {
			fleet.LaunchTemplateMap[fmt.Sprintf("%s.%s.%s", size, os, args.Label)], err = ec2.NewLaunchTemplate(
				ctx, fmt.Sprintf("%s-%s-%s", size, os, args.Label), &ec2.LaunchTemplateArgs{
					Name:                              pulumi.Sprintf("%s.%s.%s", size, os, args.Label),
					ImageId:                           pulumi.String(ami),
					InstanceType:                      pulumi.String(instanceType),
					InstanceInitiatedShutdownBehavior: pulumi.String("terminate"),
					VpcSecurityGroupIds: pulumi.StringArray{
						fleet.SecurityGroupForFleet.ID(),
					},
					BlockDeviceMappings: ec2.LaunchTemplateBlockDeviceMappingArray{
						ec2.LaunchTemplateBlockDeviceMappingArgs{
							DeviceName: pulumi.String("/dev/xvda"),
							Ebs: ec2.LaunchTemplateBlockDeviceMappingEbsArgs{
								DeleteOnTermination: pulumi.String("true"),
								Encrypted:           pulumi.String("true"),
								KmsKeyId:            fleet.KmsKey.Arn,
								VolumeType:          pulumi.String("gp3"),
								VolumeSize:          pulumi.Int(20),
							},
						},
					},
					UserData: pulumi.String(UserDataByOsMap[os]),
					Tags: pulumi.StringMap{
						"Name":  pulumi.Sprintf("%s.%s.%s", size, os, args.Label),
						"Fleet": pulumi.String(args.Label),
					},
					TagSpecifications: ec2.LaunchTemplateTagSpecificationArray{
						ec2.LaunchTemplateTagSpecificationArgs{
							Tags: pulumi.StringMap{
								"Name":      pulumi.Sprintf("%s.%s.%s", size, os, args.Label),
								"Fleet":     pulumi.String(args.Label),
								"FleetOS":   pulumi.String(os),
								"FleetSize": pulumi.String(size),
							},
						},
					},
				},
			)
			if err != nil {
				return nil, err
			}
		}
	}

	// Create fleet groups on ASGs
	for _, machine := range args.Machines {
		fleet.AsgMap[fmt.Sprintf("%s.%s.%s", machine.Size, machine.OS, args.Label)], err = autoscaling.NewGroup(
			ctx, fmt.Sprintf("%s-%s-%s", machine.Size, machine.OS, args.Label), &autoscaling.GroupArgs{
				Name: pulumi.Sprintf("%s.%s.%s", machine.Size, machine.OS, args.Label),
				TargetGroupArns: pulumi.StringArray{
					fleet.LoadBalancerTargetGroup.ID(),
				},
				AvailabilityZones: args.SubnetIdsFleet,
				LaunchTemplate: &autoscaling.GroupLaunchTemplateArgs{
					Id:      fleet.LaunchTemplateMap[fmt.Sprintf("%s.%s.%s", machine.Size, machine.OS, args.Label)].ID(),
					Version: pulumi.String("$Latest"),
				},
				MinSize:         pulumi.Int(machine.Count),
				MaxSize:         pulumi.Int(machine.Count),
				DesiredCapacity: pulumi.Int(machine.Count),
				Tags: autoscaling.GroupTagArray{
					autoscaling.GroupTagArgs{
						Key:               pulumi.String("LaunchedViaAsgName"),
						Value:             pulumi.Sprintf("%s.%s.%s", machine.Size, machine.OS, args.Label),
						PropagateAtLaunch: pulumi.Bool(true),
					},
				},
			},
		)
		if err != nil {
			return nil, err
		}
	}

	if err := ctx.RegisterResourceOutputs(fleet, pulumi.Map{}); err != nil {
		return nil, err
	}

	return fleet, nil
}
