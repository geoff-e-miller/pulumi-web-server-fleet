# Fleet

This package contains Go code for managing a fleet of web servers using AWS services.

## Contents

- `InstanceSizeMap`: A map that defines the mapping between human-readable sizes and AWS-specific instance sizes.
- `AmiIdMap`: A map that defines the mapping between operating systems (OS) and the corresponding Amazon Machine Image (AMI) IDs.
- `UserDataByOsMap`: A map that holds the base64-encoded user data for each OS.
- `WebServerFleetArgs`: Holds the arguments for creating a new web server fleet.
- `WebServerFleet`: Represents the state of the created resources for the fleet.
- `FleetConfig`: Configuration for a fleet of machines.
- `MachineConfig`: Configuration of a single machine in a fleet.
- `NewWebServerFleet`: Creates a new web server fleet with the given arguments.

## Usage

To use this package, you can import it into your Go code and use the provided functions and types to create and manage a fleet of web servers.

For example, you can create a new web server fleet by calling the `NewWebServerFleet` function and providing the required arguments:

```go
args := &fleet.WebServerFleetArgs{
    Label:           fleetConfig.Label,
    VpcId:           vpc.Vpc.ID(),
    SubnetIdsPublic: vpc.PublicSubnetIDs,
    SubnetIdsFleet:  vpc.PrivateSubnetIDs,
    Machines:        fleetConfig.Machines,
}

fleet, err := fleet.NewWebServerFleet(ctx, fleetConfig.Label, args)
if err != nil {
    return err
}

// Use the created fleet and its resources as needed
```

# User Data - Linux

This shell script is used as the user data for Linux-based machines in the web server fleet.

## Usage

The script performs the following actions:

- Checks if Docker is installed and installs it if necessary.
- Pulls the Docker Hello World image.
- Runs a Docker container using the Hello World image.
- Creates a simple index.html file.
- Copies the index.html file to the Docker container.

You can use this script as the user data for Linux-based machines to set up the required environment and run the Docker container.

Please note that you may need to modify the script to fit your specific needs, such as changing the Docker image or customizing the index.html content.

# User Data - Windows

This PowerShell script is used as the user data for Windows-based machines in the web server fleet.

## Usage

The script performs the following actions:

- Installs Docker.
- Restarts the shell or machine (optional).
- Pulls the Docker Hello World image.
- Runs a Docker container using the Hello World image.
- Creates a simple index.html file.
- Copies the index.html file to the Docker container.

You can use this script as the user data for Windows-based machines to set up the required environment and run the Docker container.

Please note that you may need to modify the script to fit your specific needs, such as changing the Docker image or customizing the index.html content.
