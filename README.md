# Project Name

Pulumi Web Server Fleet

## Table of Contents

- [Installation](#installation)
- [Configuration](#configuration)
- [Getting Started](#getting-started)
- [Deploying the Infrastructure](#deploying-the-infrastructure)
- [Cleaning Up](#cleaning-up)
- [Additional Information](#additional-information)
- [License](#license)
- [Contributors](#contributors)
- [Acknowledgments](#acknowledgments)

## Installation

To run this Pulumi project, you need to have the following dependencies and tools installed:

- Pulumi CLI: [Install Pulumi](https://www.pulumi.com/docs/get-started/install/)
- AWS CLI: [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

## Configuration

This Pulumi project uses the following configuration variables:

- `aws:region`: (Required) The AWS region where the infrastructure will be deployed.
- `network:name`: (Required) The name of the VPC network.
- `network:vpcCidr`: (Required) The CIDR block for the VPC.
- `network:azs`: (Required) An array of availability zones with their respective configuration.

Example configuration:

```yaml
config:
  aws:region: us-west-2
  network:
    name: myfleet
    VpcCidr: 10.0.0.0/16
    azs:
      - ZoneID: usw2-az1
        SubnetCidrPublic: 10.0.0.0/24
        SubnetCidrPrivate: 10.0.3.0/24
      - ZoneID: usw2-az2
        SubnetCidrPublic: 10.0.1.0/24
        SubnetCidrPrivate: 10.0.4.0/24
      - ZoneID: usw2-az3
        SubnetCidrPublic: 10.0.2.0/24
        SubnetCidrPrivate: 10.0.5.0/24
  fleet:
    label: myfleet
    subnet_layer: private
    machines:
      - os: windows
        size: medium
        count: 2
      - os: linux
        size: small
        count: 4
```

## Getting Started

To get started with this Pulumi project, follow these steps:

1. Clone the repository: `git clone <repository-url>`
2. Change into the project directory: `cd pulumi-web-server-fleet`
3. Install the project dependencies: `npm install`
4. Initialize the Pulumi project: `pulumi stack init <stack-name>`
5. Configure the required Pulumi stack settings using the `pulumi config` command. Set values for `aws:region`, `network:name`, `network:vpcCidr`, and `network:azs`.
6. Deploy the infrastructure: `pulumi up`
7. Access the deployed web server fleet using the provided output URL.

## Deploying the Infrastructure

To deploy the infrastructure, use the following command:

```
pulumi up
```

This command will create the necessary resources in the specified AWS region based on the provided configuration.

## Cleaning Up

To remove the deployed infrastructure and clean up all resources, use the following command:

```
pulumi destroy
```

**Note:** This command will delete all resources created by the Pulumi project, so use it with caution.

## License

This project is licensed under the [MIT License](LICENSE).

## Contributors

- Geoff Miller

---

Please make sure to update the

 links, contributors, and acknowledgments sections with the appropriate information for your project.

## Real World Notes

This was just an exercise. Should this have been real, I would have done somethings differently.

I would have separated the network from the fleet components into different repositories and states. If this was a module to be used in a Platform Development Kit or a CDK that is built for a specific organization's platform, we can simplify the configuration of all projects that are to be deployed into that platform. In this case, I would do an opinionated lookup of the appropriate VPC to deploy to and the appropriate subnet layer within that VPC instead of asking for it as a configuration parameter. If this project were to be used generically on any network, you would need to ask for the subnets in the way shown in the assignment description.

I would also separate the tagging package into its own repo for use on all projects.

I'm relying on the built-in public DNS being assigned to the ALB by AWS to get TLS and DNS for free. But in the real world, I would have to use route53 for the DNS and then use the ACM to place a cert on the listener for the ALB.

In this exercise, there is no TLS between the ALB and the EC2 instance. That is because ACM only works on AWS fully managed resources, like load balancers and Cloudfront. I would normally address that with certs deployed in Secrets Manager that are grabbed and installed on startup. And/Or I would look at using ACMPCA (aws private cert authority) to generate such certs and I would use route53 private hosted zones to create a private DNS structure.

I would normally create an IAM instance profile for the EC2s. The instance profile would need permissions to pull from Secrets Manager and parameter store in the path where this project's secrets and config will be stored. It will also need decrypt permissions for the KMS key used to encrypt the secrets.

I would normally give the NAT Gateways EIPs.

I would normally put a resource policy on a KMS key for security reasons.

For the user data, I wouldn't install Docker and then stand it up. I would instead know that docker was installed because I would use an AMI defined for the use case. I do not like installing anything on startup. It introduces opportunities for upstream dependencies to fail and cause the startup to fail. It also takes too much time and reduces elasticity. I believe in immutable artifacts for infrastructure. The only thing they should do on startup is to download the last-mille config and secrets and then template out config, certs, etc and then start the service. This will be faster, more secure and more stable.

## Additional Thoughts

I wanted to make a reusable tagging transformation that could be a great aid to making future projects. I don't think I have it in its final form yet. But I think it does a great job. I think it could be included as part of the AWS provider. I would take out the git stuff I was playing with before putting it in the AWS provider but... About the git tagging functions, I wanted to experiment with that as well. I think it could be useful to know the git repo that was responsible for a resource. This is a concept I'm still playing with but I thought it would be fun to include in this.
