// Package tagging provides functions for working with tags in AWS resources.
package tagging

import (
	"fmt"
	"reflect"

	"github.com/pulumi/pulumi-aws/sdk/v5/go/aws/autoscaling"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// RegisterAutoTags registers a global stack transformation that merges a set
// of tags with whatever was also explicitly added to the resource definition.
func RegisterAutoTags(ctx *pulumi.Context, autoTags map[string]string) {
	AddGitRemoteTag(ctx, autoTags)

	ctx.RegisterStackTransformation(
		func(args *pulumi.ResourceTransformationArgs) *pulumi.ResourceTransformationResult {
			if IsTaggable(args) {
				ptr := reflect.ValueOf(args.Props)
				val := ptr.Elem()
				tags := val.FieldByName("Tags")

				var tagsMap pulumi.StringMap
				var groupTagArray autoscaling.GroupTagArray

				// Most Tags are tagsMaps. This is what you'd normally expect
				// if reflect.TypeOf(tags) == reflect.TypeOf(tagsMap) {
				if _, ok := tags.Interface().(pulumi.StringMap); ok {
					ctx.Log.Debug("RegisterAutoTags: Handling tags for pulumi.StringMap", nil)
					if !tags.IsZero() {
						tagsMap = tags.Interface().(pulumi.StringMap)
					} else {
						tagsMap = pulumi.StringMap{}
					}
					for k, v := range autoTags {
						tagsMap[k] = pulumi.String(v)
						ctx.Log.Debug(fmt.Sprintf("RegisterAutoTags: Added tag for pulumi.StringMap >> %s: %s", k, v), nil)
					}
					tags.Set(reflect.ValueOf(tagsMap))
					// Autoscaling Groups have a different Tag setup
				} else if _, ok := tags.Interface().(autoscaling.GroupTagArray); ok {
					ctx.Log.Debug("RegisterAutoTags: Handling tags for autoscaling.GroupTagArray", nil)
					if !tags.IsZero() {
						groupTagArray = tags.Interface().(autoscaling.GroupTagArray)
					} else {
						groupTagArray = autoscaling.GroupTagArray{}
					}
					for k, v := range autoTags {
						groupTagArray = append(groupTagArray, autoscaling.GroupTagArgs{
							Key:               pulumi.String(k),
							Value:             pulumi.String(v),
							PropagateAtLaunch: pulumi.Bool(true),
						})
						ctx.Log.Debug(fmt.Sprintf("RegisterAutoTags: Added tag for autoscaling.GroupTagArray >> %s: %s", k, v), nil)
					}
					tags.Set(reflect.ValueOf(groupTagArray))
				} else {
					interfaceType := reflect.TypeOf(tags.Interface()).String()
					ctx.Log.Warn(fmt.Sprintf("RegisterAutoTags: Unsupported tags interface type: %s", interfaceType), nil)
				}

				return &pulumi.ResourceTransformationResult{
					Props: args.Props,
					Opts:  args.Opts,
				}
			}
			return nil
		},
	)
}

// IsTaggable returns true if the given resource type is an AWS resource that supports tags.
func IsTaggable(args *pulumi.ResourceTransformationArgs) bool {
	fieldName := "Tags"

	if args.Props == nil {
		return false
	}

	val := reflect.ValueOf(args.Props)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	return val.FieldByName(fieldName).IsValid()
}
