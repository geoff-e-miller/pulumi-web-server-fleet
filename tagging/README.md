# Tagging Package

The `tagging` package provides functions for working with tags in AWS resources.

## RegisterAutoTags

The `RegisterAutoTags` function registers a global stack transformation that merges a set of tags with whatever was also explicitly added to the resource definition.

### Signature

```go
func RegisterAutoTags(ctx *pulumi.Context, autoTags map[string]string)
```

### Parameters

- `ctx`: The Pulumi context.
- `autoTags`: A map of key-value pairs representing the tags to be added.

### Description

The `RegisterAutoTags` function registers a stack transformation that automatically adds the specified tags to AWS resources. It performs the following steps:

1. Checks if the resource is taggable by calling the `IsTaggable` function.
2. If the resource is taggable, it retrieves the `Tags` field from the resource.
3. If the `Tags` field is of type `pulumi.StringMap`, the function adds the tags to the existing tags map.
4. If the `Tags` field is of type `autoscaling.GroupTagArray`, the function appends the tags to the existing group tags array.
5. If the `Tags` field is of any other type, the function logs a warning message indicating the type of the `Tags` field and does not modify the tags.

### IsTaggable

The `IsTaggable` function determines whether the given resource type is an AWS resource that supports tags.

### Signature

```go
func IsTaggable(args *pulumi.ResourceTransformationArgs) bool
```

### Parameters

- `args`: The resource transformation arguments.

### Returns

- `true` if the resource type supports tags, `false` otherwise.

### Description

The `IsTaggable` function checks if the `Tags` field is present in the resource and returns `true` if it is, indicating that the resource supports tags.

## Usage

To use the `tagging` package, import it into your Pulumi project:

```go
import "your-module-path/tagging"
```

Then, you can use the `RegisterAutoTags` function to automatically add tags to your AWS resources.

```go
tagging.RegisterAutoTags(ctx, autoTags)
```

Make sure to pass the Pulumi context (`ctx`) and a map of key-value pairs representing the tags you want to add (`autoTags`).

Note: Ensure that your AWS resources have a `Tags` field defined for the tags to be added correctly.

## Example

Here's an example that demonstrates how to use the `tagging` package to add tags to an EC2 instance:

```go
// Create an EC2 instance
instance, err := ec2.NewInstance(ctx, "my-instance", &ec2.InstanceArgs{
    // Instance configuration...
    Tags: pulumi.Map{}, // Empty tags map
})
if err != nil {
    panic(err)
}

// Register auto tags
autoTags := map[string]string{
    "Project": "MyProject",
    "Environment": "Production",
}
tagging.RegisterAutoTags(ctx, autoTags)
```

In this example, the `RegisterAutoTags` function adds the specified tags to the `Tags` field of the EC2 instance.

## Contributing

Contributions to the `tagging` package are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request on the [GitHub repository](link-to-repo).
```

Please note that you may need to update the "your-module-path" and "link-to-repo" placeholders with the appropriate values for your
