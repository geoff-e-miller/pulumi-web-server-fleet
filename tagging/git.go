package tagging

import (
	"fmt"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

func AddGitRemoteTag(ctx *pulumi.Context, autoTags map[string]string) {
	repo, err := git.PlainOpen(".")
	if err != nil {
		ctx.Log.Warn("Failed to open Git repository: "+err.Error(), nil)
		return
	}

	remote, err := GetGitRemote(repo)
	if err != nil {
		ctx.Log.Warn("Failed to retrieve Git remote: "+err.Error(), nil)
		return
	}

	autoTags["iac.pulumi.git.remote"] = remote
}

func GetGitRemote(repo *git.Repository) (string, error) {
	cfg, err := repo.Config()
	if err != nil {
		return "", err
	}

	for _, remoteConfig := range cfg.Remotes {
		if remoteConfig.Name == "origin" {
			if len(remoteConfig.URLs) > 0 {
				// Strip any credentials from the URL
				url := strings.ReplaceAll(remoteConfig.URLs[0], "https://", "http://")
				return url, nil
			}
		}
	}

	return "", fmt.Errorf("Failed to retrieve Git remote")
}
