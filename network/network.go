package network

type AZConfig struct {
	ZoneID            string `json:"ZoneID"`
	SubnetCidrPublic  string `json:"SubnetCidrPublic"`
	SubnetCidrPrivate string `json:"SubnetCidrPrivate"`
}

type NetworkConfig struct {
	Name    string     `json:"name"`
	VpcCidr string     `json:"VpcCidr"`
	AZs     []AZConfig `json:"azs"`
}
