package network

import (
	"fmt"

	"github.com/pulumi/pulumi-aws/sdk/v5/go/aws/ec2"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

type VPCArgs struct {
	Name    string
	VpcCidr string
	AZs     []AZArgs
}

type VPC struct {
	pulumi.ResourceState
	PublicSubnetIDs  pulumi.StringArray
	PrivateSubnetIDs pulumi.StringArray
	Vpc              *ec2.Vpc
	InternetGateway  *ec2.InternetGateway
	PublicRouteTable *ec2.RouteTable
}

func NewVPC(ctx *pulumi.Context, name string, args *VPCArgs, opts ...pulumi.ResourceOption) (*VPC, error) {
	if args.Name == "" || args.VpcCidr == "" || len(args.AZs) == 0 {
		ctx.Log.Error("Missing required input parameters: 'Name', 'VpcCidr' and at least one 'AZ'", nil)
		return nil, nil
	}

	vpc := &VPC{}
	err := ctx.RegisterComponentResource("network:index:VPC", name, vpc, opts...)
	if err != nil {
		return nil, err
	}

	vpc.Vpc, err = ec2.NewVpc(ctx, args.Name, &ec2.VpcArgs{
		CidrBlock: pulumi.String(args.VpcCidr),
		Tags: pulumi.StringMap{
			"Name": pulumi.String(args.Name),
		},
	}, pulumi.Parent(vpc))
	if err != nil {
		return nil, err
	}

	vpc.InternetGateway, err = ec2.NewInternetGateway(ctx, fmt.Sprintf("%s.igw", args.Name), &ec2.InternetGatewayArgs{
		Tags: pulumi.StringMap{
			"Name": pulumi.String(args.Name),
		},
	}, pulumi.Parent(vpc.Vpc))
	if err != nil {
		return nil, err
	}

	_, err = ec2.NewInternetGatewayAttachment(ctx, fmt.Sprintf("%s.igw.attachment", args.Name), &ec2.InternetGatewayAttachmentArgs{
		VpcId:             vpc.Vpc.ID(),
		InternetGatewayId: vpc.InternetGateway.ID(),
	}, pulumi.Parent(vpc.InternetGateway))
	if err != nil {
		return nil, err
	}

	vpc.PublicRouteTable, err = ec2.NewRouteTable(ctx, fmt.Sprintf("public.%s", args.Name), &ec2.RouteTableArgs{
		VpcId: vpc.Vpc.ID(),
		Tags: pulumi.StringMap{
			"Name": pulumi.Sprintf("public.%s", args.Name),
		},
	}, pulumi.Parent(vpc.Vpc))
	if err != nil {
		return nil, err
	}

	_, err = ec2.NewRoute(ctx, fmt.Sprintf("public.%s-igw-route", args.Name), &ec2.RouteArgs{
		RouteTableId:         vpc.PublicRouteTable.ID(),
		DestinationCidrBlock: pulumi.String("0.0.0.0/0"),
		GatewayId:            vpc.InternetGateway.ID(),
	}, pulumi.Parent(vpc.PublicRouteTable))
	if err != nil {
		return nil, err
	}

	publicSubnetIDs := pulumi.StringArray{}
	privateSubnetIDs := pulumi.StringArray{}

	for _, az := range args.AZs {
		azComponent, err := NewAZ(ctx, az.ZoneID, &AZArgs{
			PublicRouteTableId: vpc.PublicRouteTable.ID(),
			VPCID:              vpc.Vpc.ID(),
			VPCName:            args.Name,
			ZoneID:             az.ZoneID,
			SubnetCidrPublic:   az.SubnetCidrPublic,
			SubnetCidrPrivate:  az.SubnetCidrPrivate,
		}, pulumi.Parent(vpc.Vpc))
		if err != nil {
			return nil, err
		}

		publicSubnetIDs = append(publicSubnetIDs, azComponent.PublicSubnet.ID())
		privateSubnetIDs = append(privateSubnetIDs, azComponent.PrivateSubnet.ID())
	}

	vpc.PublicSubnetIDs = publicSubnetIDs
	vpc.PrivateSubnetIDs = privateSubnetIDs

	if err := ctx.RegisterResourceOutputs(vpc, pulumi.Map{
		"publicSubnetIDs":  publicSubnetIDs,
		"privateSubnetIDs": privateSubnetIDs,
	}); err != nil {
		return nil, err
	}

	return vpc, nil
}
