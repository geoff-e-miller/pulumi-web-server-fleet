package network

import (
	"fmt"

	"github.com/pulumi/pulumi-aws/sdk/v5/go/aws/ec2"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// AZArgs represents the input arguments for creating an Availability Zone (AZ) within a VPC.
type AZArgs struct {
	PublicRouteTableId pulumi.StringInput
	VPCID              pulumi.StringInput
	VPCName            string
	ZoneID             string
	SubnetCidrPublic   string
	SubnetCidrPrivate  string
}

// AZ represents an Availability Zone (AZ) within a VPC, including the associated resources.
type AZ struct {
	pulumi.ResourceState
	NatGateway              *ec2.NatGateway
	PublicSubnet            *ec2.Subnet
	PrivateSubnet           *ec2.Subnet
	PrivateSubnetRouteTable *ec2.RouteTable
}

// NewAZ creates a new Availability Zone (AZ) within a VPC and returns an instance of AZ.
func NewAZ(ctx *pulumi.Context, name string, args *AZArgs, opts ...pulumi.ResourceOption) (*AZ, error) {
	az := &AZ{}
	err := ctx.RegisterComponentResource("network:index:AZ", name, az, opts...)
	if err != nil {
		return nil, err
	}

	az.PublicSubnet, err = ec2.NewSubnet(ctx, fmt.Sprintf("%s.public.%s", args.ZoneID, args.VPCName), &ec2.SubnetArgs{
		VpcId:     args.VPCID,
		CidrBlock: pulumi.String(args.SubnetCidrPublic),
		Tags: pulumi.StringMap{
			"Name":       pulumi.Sprintf("%s.public.%s", args.ZoneID, args.VPCName),
			"SubnetType": pulumi.String("public"),
		},
	}, pulumi.Parent(az))
	if err != nil {
		return nil, err
	}

	az.NatGateway, err = ec2.NewNatGateway(ctx, fmt.Sprintf("%s.public.%s", args.ZoneID, args.VPCName), &ec2.NatGatewayArgs{
		SubnetId: az.PublicSubnet.ID(),
		Tags: pulumi.StringMap{
			"Name": pulumi.Sprintf("%s.private.%s", args.ZoneID, args.VPCName),
		},
	}, pulumi.Parent(az.PublicSubnet))
	if err != nil {
		return nil, err
	}

	az.PrivateSubnet, err = ec2.NewSubnet(ctx, fmt.Sprintf("%s.private.%s", args.ZoneID, args.VPCName), &ec2.SubnetArgs{
		VpcId:     args.VPCID,
		CidrBlock: pulumi.String(args.SubnetCidrPrivate),
		Tags: pulumi.StringMap{
			"Name":       pulumi.Sprintf("%s.private.%s", args.ZoneID, args.VPCName),
			"SubnetType": pulumi.String("private"),
		},
	}, pulumi.Parent(az))
	if err != nil {
		return nil, err
	}

	az.PrivateSubnetRouteTable, err = ec2.NewRouteTable(ctx, fmt.Sprintf("%s.private.%s", args.ZoneID, args.VPCName), &ec2.RouteTableArgs{
		VpcId: args.VPCID,
		Tags: pulumi.StringMap{
			"Name": pulumi.Sprintf("%s.private.%s", args.ZoneID, args.VPCName),
		},
	}, pulumi.Parent(az.PrivateSubnet))
	if err != nil {
		return nil, err
	}

	_, err = ec2.NewRoute(ctx, fmt.Sprintf("%s.private.%s-nat-route", args.ZoneID, args.VPCName), &ec2.RouteArgs{
		RouteTableId:         az.PrivateSubnetRouteTable.ID(),
		DestinationCidrBlock: pulumi.String("0.0.0.0/0"),
		NatGatewayId:         az.NatGateway.ID(),
	}, pulumi.Parent(az.PrivateSubnetRouteTable))
	if err != nil {
		return nil, err
	}

	if err := ctx.RegisterResourceOutputs(az, pulumi.Map{}); err != nil {
		return nil, err
	}

	return az, nil
}
