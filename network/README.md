# Network

This package contains Go code for managing the network infrastructure required for the web server fleet.

## Contents

- `AZConfig`: Holds the configuration for an Availability Zone (AZ).
- `NetworkConfig`: Holds the configuration for the network.
- `NewVPC`: Creates a new virtual private cloud (VPC) with the given arguments.

# Virtual Private Cloud (VPC)

This Go code defines the creation and management of a virtual private cloud (VPC) for the web server fleet.

## Contents

- `VPCArgs`: Holds the arguments for creating a new VPC.
- `VPC`:  Represents the state of the created resources for the VPC.
- `NewVPC`: Creates a new VPC with the given arguments.

## Usage

To use this code, you can import it into your Go code and use the provided functions and types to create and manage a VPC for the web server fleet.

For example, you can create a new VPC by calling the `NewVPC` function and providing the required arguments:

```go
networkConfig := &network.NetworkConfig{}
conf := config.New(ctx, "")
conf.RequireObject("network", networkConfig)

azs := make([]network.AZArgs, len(networkConfig.AZs))
for i, az := range networkConfig.AZs {
    azs[i] = network.AZArgs{
        ZoneID:            az.ZoneID,
        SubnetCidrPublic:  az.SubnetCidrPublic,
        SubnetCidrPrivate: az.SubnetCidrPrivate,
    }
}

vpc, err := network.NewVPC(ctx, networkConfig.Name, &network.VPCArgs{
    Name:    networkConfig.Name,
    VpcCidr: networkConfig.VpcCidr,
    AZs:     azs,
})
if err != nil {
    return err
}

// Use the created VPC and its resources as needed
```

# Availability Zone (AZ)

This Go code defines the creation and management of an Availability Zone (AZ) within a virtual private cloud (VPC).

## Contents

- `AZArgs`: Holds the arguments for creating a new AZ.
- `AZ`:  Represents the state of the created resources for the AZ.
- `NewAZ`: Creates a new AZ with the given arguments.

## Usage

To use this code, you can import it into your Go code and use the provided functions and types to create and manage AZs within a VPC.

For example, you can create a new AZ by calling the `NewAZ` function and providing the required arguments:

```go
_, err := NewAZ(ctx, az.ZoneID, &AZArgs{
    PublicRouteTableId: vpc.PublicRouteTable.ID(),
    VPCID:              vpc.Vpc.ID(),
    VPCName:            args.Name,
    ZoneID:             az.ZoneID,
    SubnetCidrPublic:   az.SubnetCidrPublic,
    SubnetCidrPrivate:  az.SubnetCidrPrivate,
}, pulumi.Parent(vpc.Vpc))
if err != nil {
    return nil, err
}
```
