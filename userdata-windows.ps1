# Configuration variables
$dockerInstallPath = 'C:\path\to\your\desired\install\location'
$dockerVersion = '19.03.5'
$wwwDirectory = "$dockerInstallPath\www"
$indexFile = "$wwwDirectory\index.html"
$containerName = 'my-hello-world'

# Install Docker
Invoke-WebRequest -UseBasicParsing -OutFile dockerInstall.ps1 -Uri https://get.docker.com/win/dockerInstall.ps1
.\dockerInstall.ps1 -InstallPath $dockerInstallPath -DockerVersion $dockerVersion

# Restart the shell or machine to ensure Docker commands work
# Restart-Computer -Force

# Pull the Docker Hello World image
docker pull httpd:2.4

# Run a Docker container using the Hello World image
docker run -d --name $containerName -p 80:80 httpd:2.4

# Create a simple index.html
if (!(Test-Path -Path $wwwDirectory)) {
    New-Item -ItemType Directory -Force -Path $wwwDirectory
}
Write-Output '<html><body><h1>Hello, World!</h1></body></html>' | Out-File -FilePath $indexFile

# Copy the index.html file to the Docker container
docker cp $indexFile ${containerName}:/usr/local/apache2/htdocs/
